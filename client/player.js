class AudioPlayer {
    constructor(context, basePath = "", stereo = true) {
        this.audio = new Audio();
        this.source = context.createMediaElementSource(this.audio);
        this.destination = context.destination;
        this.basePath = basePath;
        this.tracklist = { 'offset': 10, 'AUSSEN': 'FRUEHSTUECK_AUSSEN_ST.ogg', 'MITTE': 'FRUEHSTUECK_INNEN_ST.ogg' }
        if (stereo) this.connect(); // directly connect source to destination
    }
    play(time) {
        return new Promise((resolve, reject) => {
            if (!this.audio.src) return reject('no audio source to play');
            if (time || time === 0) this.currentTime = time;
            this.audio.onerror = reject;
            this.audio.onended = () => {
                this.currentTime = 0;
                return resolve('played');
            };
            logInfo('AudioPlayer', 'play', time)
            this.audio.play();
        });
    }
    pause() {
        logInfo('AudioPlayer', 'pause')
        this.audio.pause();
    }
    stop() {
        logInfo('AudioPlayer', 'stop')
        this.audio.pause();
        this.currentTime = 0;
    }
    connect() {
        this.source.connect(this.destination);
    }
    disconnect() {
        if (!this.destination) return false
        return this.source.disconnect(this.destination);
    }
    set track(key) {
        if (!this.tracklist.hasOwnProperty(key)) {
            logError('no track key:', key, this.tracklist);
            return false
        }
        else {
            logInfo('AudioPlayer', 'new track', key)
            this.audio.src = this.basePath + this.tracklist[key];
            return this.audio.src
        }
    }
    set src(path) {
        logInfo('AudioPlayer', 'new src', path)
        this.audio.src = this.basePath + path;
        return this.audio.src
    }
    set volume(v) {
        logInfo('AudioPlayer', 'new volume', v)
        this.audio.volume = v;
    }
    set currentTime(time) {
        logInfo('AudioPlayer', 'currentTime', time)
        this.audio.currentTime = time;
        return this.audio.currentTime;
    }
    get isPlaying() {
        return !this.audio.paused;
    }
    get currentTime() {
        return this.audio.currentTime;
    }
    getDuration() {
        return new Promise((resolve, reject) => {
            this.audio.onloadedmetadata = function(e){
                logInfo("AudioPlayer",'Metadata loaded')
                resolve(e.target.duration);
            };
        });
    }
}

class AmbixPlayer extends AudioPlayer {
    constructor(context, basePath) {
        super(context, basePath, false);
        this.mirrorNode = new ambisonics.sceneMirror(context, 1);
        this.rotatorNode = new ambisonics.sceneRotator(context, 1);
        this.decoderNode = new ambisonics.binDecoder(context, 1);
        this.gainNode = context.createGain();
        this.source.connect(this.mirrorNode.in);
        this.mirrorNode.out.connect(this.rotatorNode.in);
        this.rotatorNode.out.connect(this.decoderNode.in);
        this.decoderNode.out.connect(this.gainNode);
        this.connect(this.destination)
    }
    connect(destination) {
        this.gainNode.connect(destination);
    }
    rotate([roll = 0, pitch = 0, yaw = 0]) {
        this.rotatorNode.roll = roll;
        this.rotatorNode.pitch = pitch;
        this.rotatorNode.yaw = yaw;
        this.rotatorNode.updateRotMtx();
        return [roll, pitch, yaw];
    }
    // planes 1: front-back, 2: left-right, 3: up-down, 0: no reflection
    mirror(plane) {
        this.mirrorNode.mirror(plane);
    }
}
