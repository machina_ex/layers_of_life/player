function localRotate(aPlayer) {
    const rollValue = Math.round(document.getElementById('inputX').value);
    const pitchValue = Math.round(document.getElementById('inputY').value);
    const yawValue = Math.round(document.getElementById('inputZ').value);
    updateInfos([rollValue, pitchValue, yawValue]);
    aPlayer.rotate([rollValue, pitchValue, yawValue]);
}

function updateInfos([roll = 0, pitch = 0, yaw = 0]) {
    document.getElementById("infoXValue").innerText = roll;
    document.getElementById("inputX").value = roll;
    document.getElementById("infoYValue").innerText = pitch;
    document.getElementById("inputY").value = pitch;
    document.getElementById("infoZValue").innerText = yaw;
    document.getElementById("inputZ").value = yaw;
}

function getActivePlayer(p1, p2) {
    let active = false;
    if (p2.isPlaying && p1.isPlaying) {
        logError('Stereo and Amix Player are both playing.');
        return false;
    }
    if (p1.isPlaying)
        active = p1;
    else if (p2.isPlaying)
        active = p2;
    return active;
}

function randomString(prefix) {
    return prefix + Math.random().toString(16).substr(2, 8);
}

function isInteger(str, x) {
    const res = str == "" ? NaN : Number(str);
    return Number.isInteger(res) ? res : x;
}

function getParameter(param) {
    const location = window.location.search;
    return new URLSearchParams(location).get(param) || undefined;
}

function findScene(k, v, arr) {
    let scene = arr.find(s => s[k] === v);
    if (scene === undefined)
        scene = arr.find(s => s["DAY"] === "DEFAULT");
    // TODO select random zonks 
    return scene
}

function logError(...error) {
    console.error(...error);
}

function logInfo(...message) {
    if (LOGLEVEL == "DEBUG") console.log(...message);
}
