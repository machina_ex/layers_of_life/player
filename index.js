#!/usr/bin/env node

const puppeteer = require("puppeteer");
const path = require("path");
const os = require("os");
const fs = require("fs");

const configPath = path.join(__dirname, "config.json");
const config = JSON.parse(
    fs.readFileSync(configPath, { encoding: "utf8", flag: "r" })
);
const hostname = os.hostname();

(async () => {
    console.log(
        `launching browser on ${hostname} with config ${JSON.stringify(config)}`
    );
    const browser = await puppeteer.launch({
        headless: true,
        // comment out or change if your browser is somewhere different e.g. on Windows
        executablePath: '/usr/bin/chromium-browser',
        ignoreDefaultArgs: ["--mute-audio"],
        args: [
            "--autoplay-policy=no-user-gesture-required",
            "--disable-web-security",
        ],
    });
    let [page] = await browser.pages();
    if (!page) page = await browser.newPage();

    page
        .on("console", (msg) =>
            console.log(`${msg.type().substr(0, 3).toUpperCase()} ${msg.text()}`)
        )
        .on("pageerror", ({ msg }) => console.log(msg))
        .on("response", (res) => console.log(`${res.status()} ${res.url()}`));
    //  .on('requestfailed', request => console.log(`${request.failure().errorText} ${request.url()}`))

    await page.goto(
        `file:${path.join(__dirname, `/client/index.html?host=${hostname}&koffer=${config.KOFFER}`)}`
    );
})();
